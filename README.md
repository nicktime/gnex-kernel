This work is abandoned.
-----------------------------

First of all, this kernel is in a very early stage, hence may fry something.
If you're not a kernel developer, you don't need it.

This kernel is tested only on Galaxy Nexus i9250 GSM version
(see [current kernel status][1] ).
Kernel config and initramfs are in [gnex-misc repo][2].

Plans:  
Stage 1: get it working with least effort on latest stable kernel branch.
See latest gnex-3.x branch.  
Stage 2: clean up and upstream. Will be on branch master.

[1]: https://bitbucket.org/nicktime/gnex-kernel/wiki/Galaxy_Nexus_kernel_status
[2]: https://bitbucket.org/nicktime/gnex-misc